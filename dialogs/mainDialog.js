// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ComponentDialog, ConfirmPrompt, TextPrompt, DialogSet, DialogTurnStatus, WaterfallDialog } = require('botbuilder-dialogs');
const { AddDialog, ADD_DIALOG } = require('./AddDialog');
const { UserProfile } = require('../userProfile');

const MAIN_DIALOG = 'MAIN_DIALOG';
const USER_PROFILE = 'USER_PROFILE';
const NAME_PROMPT = 'NAME_PROMPT';
const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
// const USER_PROFILE_PROPERTY = 'USER_PROFILE_PROPERTY';

class MainDialog extends ComponentDialog {
    constructor(userState) {
        super(MAIN_DIALOG);
        this.userState = userState;
        this.userProfile = userState.createProperty(USER_PROFILE);

        this.addDialog(new AddDialog(userState));
        this.addDialog(new TextPrompt(NAME_PROMPT, this.namePromptValidator));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.initialStep.bind(this),
            this.confirmUserNameStep.bind(this),
            this.finalStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    async initialStep(step) {
        const userProfile = await this.userProfile.get(step.context, new UserProfile());
        if (userProfile.name) {
            await step.context.sendActivity(`Hello again ${ userProfile.name }.`);
            return step.next();
        } else {
            const promptOptions = { prompt: 'Please enter your name.', retryPrompt: 'The value entered must be greater than 3 and less than 30.' };
            return await step.prompt(NAME_PROMPT, promptOptions);
        }
    }

    async confirmUserNameStep(step) {
        if (step.result) {
            const userProfile = await this.userProfile.get(step.context, new UserProfile());
            userProfile.name = step.result;

            // We can send messages to the user at any point in the WaterfallStep.
        }

        // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is a Prompt Dialog.
        return await step.prompt(CONFIRM_PROMPT, 'Do you want add two numbers?', ['yes', 'no']);
    }

    async finalStep(step) {
        const userProfile = await this.userProfile.get(step.context, new UserProfile());
        // if (step.result) {
        //     return await step.beginDialog(ADD_DIALOG);
        // } else {
        //     // User said "no" so we will skip the next step. Give -1 as the age.
        //     return await step.endDialog();
        // }
        return step.result ? await step.beginDialog(ADD_DIALOG, userProfile.name) : await step.endDialog();
    }

    async namePromptValidator(promptContext) {
        // This condition is our validation rule. You can also change the value at this point.
        return promptContext.recognized.succeeded && promptContext.recognized.value.length >= 3 && promptContext.recognized.value.length <= 30;
    }
}

module.exports.MainDialog = MainDialog;
module.exports.MAIN_DIALOG = MAIN_DIALOG;
