// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// const { MessageFactory } = require('botbuilder');
const {
    ChoicePrompt,
    ComponentDialog,
    ConfirmPrompt,
    DialogSet,
    DialogTurnStatus,
    NumberPrompt,
    TextPrompt,
    WaterfallDialog
} = require('botbuilder-dialogs');

// const { UserProfile } = require('../userProfile');
const { InputDialog } = require('./InputDialog');

const CHOICE_PROMPT = 'CHOICE_PROMPT';
const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const NAME_PROMPT = 'NAME_PROMPT';
const NUMBER_PROMPT = 'NUMBER_PROMPT';
const FIRST_PROMPT = 'FIRST_PROMPT';
const SECOND_PROMPT = 'SECOND_PROMPT';
// const USER_PROFILE = 'USER_PROFILE';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
const ADD_DIALOG = 'ADD_DIALOG';

class AddDialog extends ComponentDialog {
    constructor() {
        super(ADD_DIALOG);
        this.addDialog(new InputDialog(FIRST_PROMPT, 'first'));
        this.addDialog(new InputDialog(SECOND_PROMPT, 'second'));
        this.addDialog(new TextPrompt(NAME_PROMPT));
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new NumberPrompt(NUMBER_PROMPT));

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.FirstInputStep.bind(this),
            this.SecondInputStep.bind(this),
            this.summaryStep.bind(this),
            this.lastConfirmStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    async FirstInputStep(step) {
        return await step.beginDialog(FIRST_PROMPT);
    }

    async SecondInputStep(step) {
        step.values.num1 = step.result;

        // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is a Prompt Dialog.
        return await step.beginDialog(SECOND_PROMPT);
    }

    async summaryStep(step) {
        step.values.num2 = step.result;
        const sum = step.values.num1 + step.values.num2;

        const msg = ` ${ step.options }, your result is
         ${ step.values.num1 } + ${ step.values.num2 } = ${ sum }`;
        await step.context.sendActivity(msg);
        // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is the end.
        return await step.prompt(CONFIRM_PROMPT, { prompt: 'Do you want to continue?' });
    }

    async lastConfirmStep(step) {
        return !step.result ? await step.endDialog() : await step.beginDialog(this.initialDialogId);
    }
}

module.exports.AddDialog = AddDialog;
module.exports.ADD_DIALOG = ADD_DIALOG;
