// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ConfirmPrompt, NumberPrompt, ComponentDialog, WaterfallDialog } = require('botbuilder-dialogs');

const INPUT_DIALOG = 'INPUT_DIALOG';

const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const NUMBER_PROMPT = 'NUMBER_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class InputDialog extends ComponentDialog {
    constructor(dialogId, desc) {
        super(dialogId);
        this.desc = desc;
        // Define value names for values tracked inside the dialogs.

        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new NumberPrompt(NUMBER_PROMPT));
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.numInputStep.bind(this),
            this.confirmInputStep.bind(this),
            this.lastInputStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async numInputStep(step) {
        // User said "yes" so we will be prompting for the age.
        // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is a Prompt Dialog.
        const promptOptions = { prompt: `Please enter your ${ this.desc } number`, retryPrompt: 'The value entered must be a number' };
        return await step.prompt(NUMBER_PROMPT, promptOptions);
    }

    async confirmInputStep(step) {
        step.values.num = step.result;

        // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is a Prompt Dialog.
        return await step.prompt(CONFIRM_PROMPT, { prompt: `your number is ${ step.values.num } this okay?` });
    }

    async lastInputStep(step) {
        const num = step.values.num;
        const choice = step.result;

        if (choice) {
            // If they're done, exit and return their list.
            return await step.endDialog(num);
        } else {
            // Otherwise, repeat this dialog, passing in the list from this iteration.
            return await step.beginDialog(this.initialDialogId);
        }
    }
}

module.exports.InputDialog = InputDialog;
module.exports.INPUT_DIALOG = INPUT_DIALOG;
