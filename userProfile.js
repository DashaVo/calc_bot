// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

class UserProfile {
    constructor(name, num1, num2) {
        this.name = name;
        this.num1 = num1;
        this.num2 = num2;
    }
}

module.exports.UserProfile = UserProfile;
